module Lib where

import Data.Map.Strict (empty, Map(..), toList, insertWith, fromListWith)
import Text.ParserCombinators.Parsec
import Data.List (foldl', map, sortBy)
import Data.Text (Text(..), toLower, pack)
import Data.Text.IO (readFile) 
import Prelude hiding (readFile, map)


mn = do  
   content <- readFile "big.txt"
   case (parseTextFile content) of
     Right rs -> rs |> concat |> map ( toLower . pack ) |> countWords |> sort |> print
     e -> print e

(|>) = flip ($)

countWords :: [Text] -> Map Text Int
countWords ts = fromListWith (+) (zip ts (repeat 1))

sort :: Map Text Int -> [(Text, Int)]
sort ts = sortBy (\ (_, a) (_, b) -> compare a b) (toList ts)

textFile = endBy line eol
line = sepBy word (many1 space')
word = many $ noneOf ( eolString ++ ignoreString )
eol = many1 $ oneOf eolString
space' = oneOf ignoreString

eolString = "\n\r"
ignoreString = " \t.,\"'-_#:;()?*!|"

parseTextFile :: Text -> Either ParseError [[String]]
parseTextFile input = parse textFile "(unknown)" input






-- read file
-- parse words
-- a) from text
-- b) from sqlite
-- word count
